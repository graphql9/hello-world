const {
  graphql,
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
} = require("graphql");

const WithGraphQLSchema = () => {
  const schema = new GraphQLSchema({
    query: new GraphQLObjectType({
      name: "helloWorld",
      description: "Hello World Query",
      fields: () => ({
        message: { type: GraphQLString, resolve: () => "Hello World!" },
        link: { type: GraphQLString, resolve: () => "www.google.com" },
      }),
    }),
  });

  // Accessing message
  graphql({ schema, source: `{ message }` }).then((response) => {
    console.log("\nRESPONSE I::: ", response);
  });

  // Accessing link
  graphql({ schema, source: `{ link }` }).then((response) => {
    console.log("RESPONSE II::: ", response);
  });

  // Accessing message and link
  graphql({ schema, source: `{ message, link }` }).then((response) => {
    console.log("RESPONSE III::: ", response);
  });
};

module.exports = WithGraphQLSchema;

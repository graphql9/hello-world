const { graphql, buildSchema } = require("graphql");

const BuildSchema = () => {
  const schema = buildSchema(`
  type Query {
    helloWorld: message
    message: String
  }

  type message {
    message: String
  }
`);

  const rootValue = {
    helloWorld: { message: () => "Hello World!" },
    message: "Hello World!",
  };

  const sourceI = "{ helloWorld { message } }";

  const sourceII = "{ message }";

  // Accessing from helloWorld
  graphql({ schema, source: sourceI, rootValue }).then((response) => {
    console.log("\nRESPONSE I::: ", response);
  });

  // Accessing message alone
  graphql({ schema, source: sourceII, rootValue }).then((response) => {
    console.log("RESPONSE II::: ", response);
  });
};

module.exports = BuildSchema;

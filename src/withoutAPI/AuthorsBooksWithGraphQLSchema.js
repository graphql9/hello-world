const { graphql } = require("graphql");
const { schema } = require("../schema/index");

const AuthorsBooksWithGraphQLSchema = () => {
  // Accessing the name of each books
  graphql({ schema, source: `{ books { name } }` }).then((response) => {
    console.log("\nRESPONSE I ::: ", response);
  });

  // Accessing the name of each books and authors
  graphql({ schema, source: `{ books { name, author { name } } }` }).then(
    (response) => {
      console.log("RESPONSE II::: ", response);
    }
  );

  // Accessing the id & name of a books using authorId
  graphql({ schema, source: `{ books(authorId: 1) { id, name } }` }).then(
    (response) => {
      console.log("RESPONSE III::: ", response);
    }
  );

  // Accessing the name of a book using id
  graphql({ schema, source: `{ book(id: 2) { name } }` }).then((response) => {
    console.log("RESPONSE IV::: ", response);
  });

  // Accessing the authorId of a book using name
  graphql({ schema, source: `{ book(name: "Real Steel") { authorId } }` }).then(
    (response) => {
      console.log("RESPONSE V::: ", response);
    }
  );

  // Accessing the name of each authors
  graphql({ schema, source: `{ authors { name } }` }).then((response) => {
    console.log("RESPONSE VI ::: ", response);
  });

  // Accessing the id of each authors
  graphql({ schema, source: `{ authors { id } }` }).then((response) => {
    console.log("RESPONSE VII ::: ", response);
  });

  // Accessing the name of each books and authors
  graphql({ schema, source: `{ authors { name, books { name } } }` }).then(
    (response) => {
      console.log("RESPONSE VIII::: ", response);
    }
  );

  // Accessing the name of authors using id
  graphql({ schema, source: `{ authors(id: 2) { name } }` }).then(
    (response) => {
      console.log("RESPONSE IX::: ", response);
    }
  );

  // Accessing the id of authors using name
  graphql({ schema, source: `{ authors(name: "James") { id } }` }).then(
    (response) => {
      console.log("RESPONSE X::: ", response);
    }
  );

  // Accessing the name of an author using id
  graphql({ schema, source: `{ author(id: 2) { name } }` }).then((response) => {
    console.log("RESPONSE XI::: ", response);
  });

  // Accessing the id of an author using name
  graphql({ schema, source: `{ author(name: "J K Rowling") { id } }` }).then(
    (response) => {
      console.log("RESPONSE XII::: ", response);
    }
  );

  // Accessing the books of an author using name
  graphql({
    schema,
    source: `{ author(name: "J K Rowling") { books { name } } }`,
  }).then((response) => {
    console.log("RESPONSE XIII::: ", response);
  });

  // Creating an author
  graphql({
    schema,
    source: `mutation { createAuthor(name: "Manikandan") { id, name } }`,
  }).then((response) => {
    console.log("RESPONSE XIV::: ", response);
  });

  // Updating an author
  graphql({
    schema,
    source: `mutation { updateAuthor(id: 3, name: "Vinoth") { name } }`,
  }).then((response) => {
    console.log("RESPONSE XV::: ", response);
  });

  // Creating a book
  graphql({
    schema,
    source: `mutation { createBook(name: "Biography", authorId: 3) { id, name } }`,
  }).then((response) => {
    console.log("RESPONSE XVI::: ", response);
  });

  // Accessing the name of a book using id
  graphql({ schema, source: `{ book(authorId: 3) { name } }` }).then(
    (response) => {
      console.log("RESPONSE XVII::: ", response);
    }
  );

  // Updating a book
  graphql({
    schema,
    source: `mutation { updateBook(id: 5, name: "Bio") { id, name } }`,
  }).then((response) => {
    console.log("RESPONSE XVIII::: ", response);
  });

  // Accessing the name of a book using id
  graphql({ schema, source: `{ book(id: 5) { name } }` }).then((response) => {
    console.log("RESPONSE XIX::: ", response);
  });
};

module.exports = AuthorsBooksWithGraphQLSchema;

const {
  GraphQLObjectType,
  GraphQLList,
  GraphQLID,
  GraphQLNonNull,
} = require("graphql");
const { errorName } = require("../api/Errors");
const { authors, books, users } = require("../utils/FakeData");
const strings = require("../utils/Strings");
const { commonArgs, bookType, authorType, userType } = require("./types");

const rootQueryType = new GraphQLObjectType({
  name: "rootQuery",
  description: strings.rootQueryDescription,
  fields: () => ({
    user: {
      type: new GraphQLNonNull(userType),
      description: strings.userQueryDescription,
      args: commonArgs,
      resolve: (_, args) => {
        try {
          const userData = users.find(
            (user) =>
              user.id == args.id ||
              user.name == args.name ||
              (user.email == args.email && user.password == args.password)
          );

          if (Boolean(userData)) {
            return userData;
          } else {
            throw new Error(errorName.USER_NOT_FOUND);
          }
        } catch (error) {
          throw error;
        }
      },
    },
    users: {
      type: new GraphQLList(userType),
      description: strings.usersQueryDescription,
      resolve: () => users,
    },
    author: {
      type: authorType,
      description: strings.authorQueryDescription,
      args: commonArgs,
      resolve: (_, args) =>
        authors.find(
          (author) => author.id == args.id || author.name == args.name
        ),
    },
    authors: {
      type: new GraphQLList(authorType),
      description: strings.authorsQueryDescription,
      args: commonArgs,
      resolve: (_, args) =>
        args?.id || args?.name
          ? authors.filter(
              (author) => author.id == args.id || author.name == args.name
            )
          : authors,
    },
    book: {
      type: bookType,
      description: strings.bookQueryDescription,
      args: {
        ...commonArgs,
        authorId: { type: GraphQLID },
      },
      resolve: (_, args) =>
        books.find(
          (book) =>
            book.id == args.id ||
            book.name == args.name ||
            book.authorId == args.authorId
        ),
    },
    books: {
      type: new GraphQLList(bookType),
      description: strings.booksQueryDescription,
      args: { authorId: { type: GraphQLID } },
      resolve: (_, args) =>
        args?.authorId
          ? books.filter((book) => book.authorId == args.authorId)
          : books,
    },
  }),
});

module.exports = { rootQueryType };

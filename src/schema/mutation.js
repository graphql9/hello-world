const {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLID,
} = require("graphql");
const { authors, books } = require("../utils/FakeData");
const strings = require("../utils/Strings");
const { authorType, bookType, commonFields } = require("./types");

const rootMutatioType = new GraphQLObjectType({
  name: "rootMutation",
  description: strings.rootMutationDescription,
  fields: () => ({
    createAuthor: {
      type: authorType,
      description: strings.createAuthorMutationDescription,
      args: { name: { type: new GraphQLNonNull(GraphQLString) } },
      resolve: (_, args) => {
        const createAuthorRequestData = {
          id: authors.length + 1,
          name: args.name,
        };

        authors.push(createAuthorRequestData);

        return createAuthorRequestData;
      },
    },
    updateAuthor: {
      type: authorType,
      description: strings.updateAuthorMutationDescription,
      args: commonFields,
      resolve: (_, args) => {
        var index = authors.findIndex((author) => author.id == args.id);

        const updateAuthorRequestData = {
          id: args.id,
          name: args.name,
        };

        authors[index] = updateAuthorRequestData;

        return updateAuthorRequestData;
      },
    },
    createBook: {
      type: bookType,
      description: strings.createBookMutationDescription,
      args: {
        name: { type: new GraphQLNonNull(GraphQLString) },
        authorId: { type: new GraphQLNonNull(GraphQLID) },
      },
      resolve: (_, args) => {
        const createBookRequestData = {
          id: books.length + 1,
          name: args.name,
          authorId: args.authorId,
        };

        books.push(createBookRequestData);

        return createBookRequestData;
      },
    },
    updateBook: {
      type: bookType,
      description: strings.updateBookMutationDescription,
      args: {
        ...commonFields,
        authorId: { type: GraphQLID },
      },
      resolve: (_, args) => {
        var index = books.findIndex((book) => book.id == args.id);

        const updateBookRequestData = {
          id: args.id,
          name: args.name,
          authorId: args?.authorId ?? books[index].authorId,
        };

        books[index] = updateBookRequestData;

        return updateBookRequestData;
      },
    },
  }),
});

module.exports = { rootMutatioType };

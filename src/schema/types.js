const {
  GraphQLObjectType,
  GraphQLList,
  GraphQLNonNull,
  GraphQLID,
  GraphQLString,
} = require("graphql");
const { books, authors } = require("../utils/FakeData");
const strings = require("../utils/Strings");

const commonFields = {
  id: { type: new GraphQLNonNull(GraphQLID) },
  name: { type: new GraphQLNonNull(GraphQLString) },
};

const commonArgs = {
  id: { type: GraphQLID },
  name: { type: GraphQLString },
  email: { type: GraphQLString },
  password: { type: GraphQLString },
};

const userType = new GraphQLObjectType({
  name: "user",
  description: strings.userTypeDescription,
  fields: () => ({
    ...commonFields,
    email: { type: new GraphQLNonNull(GraphQLString) },
    password: { type: new GraphQLNonNull(GraphQLString) },
  }),
});

const authorType = new GraphQLObjectType({
  name: "author",
  description: strings.authorTypeDescription,
  fields: () => ({
    ...commonFields,
    books: {
      type: new GraphQLList(bookType),
      resolve: (author) => books.filter((book) => book.authorId == author.id),
    },
  }),
});

const bookType = new GraphQLObjectType({
  name: "book",
  description: strings.bookTypeDescription,
  fields: () => ({
    ...commonFields,
    authorId: { type: new GraphQLNonNull(GraphQLID) },
    author: {
      type: authorType,
      resolve: (book) => authors.find((author) => author.id == book.authorId),
    },
  }),
});

module.exports = { authorType, bookType, commonFields, commonArgs, userType };

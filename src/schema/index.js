const { GraphQLSchema } = require("graphql");
const { rootMutatioType } = require("./mutation");
const { rootQueryType } = require("./query");

const schema = new GraphQLSchema({
  query: rootQueryType,
  mutation: rootMutatioType,
});

module.exports = { schema };

const strings = {
  // Mutation Strings
  rootMutationDescription: "Root Mutation",
  createAuthorMutationDescription: "Create an author",
  updateAuthorMutationDescription: "Update an author",
  createBookMutationDescription: "Create a book",
  updateBookMutationDescription: "Update a book",

  // Query Strings
  rootQueryDescription: "Root Query",
  authorQueryDescription: "Single author",
  authorsQueryDescription: "List of authors",
  bookQueryDescription: "Single book",
  booksQueryDescription: "List of books",
  userQueryDescription: "Single user",
  usersQueryDescription: "List of users",

  // Types Strings
  authorTypeDescription: "Author details with books",
  bookTypeDescription: "Book details with author",
  userTypeDescription: "User details",
};

module.exports = strings;

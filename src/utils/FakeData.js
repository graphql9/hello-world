const users = [
  {
    id: 1,
    name: "Manikandan S",
    email: "manikandan@gmail.com",
    password: "qwerty123",
  },
];

const books = [
  { id: 1, name: "Harry Potter", authorId: 1 },
  { id: 2, name: "Jurassic World", authorId: 2 },
  { id: 3, name: "Real Steel", authorId: 1 },
  { id: 4, name: "Titanic", authorId: 2 },
];

const authors = [
  { id: 1, name: "J K Rowling" },
  { id: 2, name: "James" },
];

module.exports = { users, books, authors };

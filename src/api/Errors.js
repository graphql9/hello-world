exports.errorName = {
  NOT_FOUND: "NOT_FOUND",
  SERVER_ERROR: "SERVER_ERROR",
  USER_NOT_FOUND: "USER_NOT_FOUND",
};

exports.errorType = {
  NOT_FOUND: {
    message: "Not Found",
  },
  SERVER_ERROR: {
    message: "Facing server issue(s)",
  },
  USER_NOT_FOUND: {
    message: "User not found",
  },
};

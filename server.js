const express = require("express");
const expressGraphQL = require("express-graphql").graphqlHTTP;
const env = require("./env");
const endpoints = require("./src/api/Endpoints");
const { errorType } = require("./src/api/Errors");
const { schema } = require("./src/schema");

const app = express();

app.use(
  `/${endpoints.graphql}`,
  expressGraphQL({
    schema,
    graphiql: true,
    customFormatErrorFn: (err) => {
      return errorType[err.message];
    },
  })
);

app.listen(env.serverPort, () => {
  console.log(`Server started running at port:${env.serverPort}`);
});

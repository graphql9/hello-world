const AuthorsBooksWithGraphQLSchema = require("./src/withoutAPI/AuthorsBooksWithGraphQLSchema");
const BuildSchema = require("./src/withoutAPI/BuildSchema");
const WithGraphQLSchema = require("./src/withoutAPI/WithGraphQLSchema");

BuildSchema();

WithGraphQLSchema();

AuthorsBooksWithGraphQLSchema();
